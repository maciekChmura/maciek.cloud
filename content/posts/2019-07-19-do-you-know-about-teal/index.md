---
title: Do you know about Teal?
date: '2019-07-19'
template: 'post'
draft: false
slug: '/do-you-know-about-teal'
category: 'Career'
tags:
  - 'Career'
description: 'How a group of people, happily united with the same goal, can cooperate to produce value'
---

TL;DR: just show me the [game](https://teal-adventure.netlify.com/).

Some time ago, while listening to a podcast, I stumbled upon a neat idea.
##Teal Organisations
Guests were talking about the structural and workforce changes happening in the tech industry.
I'm generally interested in startups and tech industry so I was intrigued about this new way how to structure a tech organization.

The idea of "Teal Organisation" was created by Frederic Laloux and described in his book: "Reinventing Organizations"

> The Teal paradigm refers to the next stage in the evolution of human consciousness. When applied to organizations, this paradigm views the organization as an independent force with its own purpose, and not merely as a vehicle for achieving management's objectives. Teal organizations are characterized by self-organization and self-management. The hierarchical "predict and control" pyramid of Orange is replaced with a decentralized structure consisting of small teams that take responsibility for their own governance and for how they interact with other parts of the organization.
> [wiki](http://reinventingorganizationswiki.com/Main_Page)

There is also a book by Polish author, Andrzej Blikle titled "Doktryna Jakosci" The [book](http://www.moznainaczej.com.pl/Download/DoktrynaJakosci/DoktrynaJako%C5%9Bci_wydanie_II.pdf) is available for free in PDF format, unfortunately, there is no English translation yet.

Teal answers a question:
###How a group of people, happily united with the same goal, can cooperate to produce value.

Some companies have a gut feeling about how to do this.

You can see Teal values in companies that were started long before Frederic Laloux published his book.

1. Basecamp -> check any book or blog post from Jason Fried and David Heinemeier Hansson and discover what they think about creating a tech company. (I might be biased because I am a huge fan) https://basecamp.com/books
2. Buffer -> https://buffer.com/about
3. Valve -> ever heard about Half-Life, Counter-Strike or Steam Platform? Check the Handbook for New Employees, its hilarious, https://www.valvesoftware.com/en/publications

Teal ideas are not some kind of revolution. You can observe them popping up everywhere.

Some of these ideas are about being [happy at work](https://www.youtube.com/watch?v=fLJsdqxnZb0).

Some of these ideas are about focusing on quality instead of [growth](https://www.amazon.com/Company-One-Staying-Small-Business/dp/1328972356).

Some of these ideas are about [trust](https://www.youtube.com/watch?v=ReRcHdeUG9Y).

I allowed myself to loosely translate 2 insightful fragments from the book from Andrzej Blikle.  
Hopefully, this will make you dig deeper into Teal.

###How to decide what to work on?
You can pick a task based on these rules:

1. you do what you can do,
2. you do what is necessary,
3. you are responsible for it,
4. what you do, you can change, but with preservation of 1, 2 and 3.

###Teal principles:

1. Do not look for the guilty one to punish - look for the cause to remove it,
2. Do not expect perfection that cannot be achieved - expect progress that is always possible,
3. Avoid competition that destroys partnership - create conditions for cooperation,
4. Do not judge, because it destroys - appreciate, because it strengthens,
5. Do not say what is wrong - say what could be better,
6. Do not ask people what they could do better - ask them what hinders their work,
7. Do not build on control - build on trust,
8. Do not say that someone is bad - say how you feel about it (the so-called "communication I"),
9. Do not manage - create conditions for self-organization,
10. Don't be a supervisor - be a teacher, moderator, and student.

As you can see all of these rules are very reasonable and natural.
To illustrate how easy it is to follow them, I made a simple [game](https://teal-adventure.netlify.com/) 😅
