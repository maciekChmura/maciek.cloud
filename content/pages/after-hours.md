---
title: "After hours"
template: "page"
slug: '/pages/after-hours'
---

I'm generally interested in tech. I thought I'd share what interest me, what I read, and how I get to expand my knowledge of programming in general.

**[Projects](/pages/projects)**. I think the best way to learn new technology is by making. Usually, I spend around one hour a day (late at night) working on my personal projects.

**Books** I like in no particular order.  
[Rework](https://www.amazon.com/Rework-Jason-Fried/dp/0307463745/)  
[It Doesn't Have to Be Crazy at Work](https://www.amazon.com/Doesnt-Have-Be-Crazy-Work/dp/0062874780)  
[Shape Up](https://basecamp.com/shapeup)  
[Hello, Startup: A Programmer's Guide to Building Products, Technologies, and Teams](https://www.amazon.com/Hello-Startup-Programmers-Building-Technologies-ebook/dp/B016YZWDA4)  
[make](https://makebook.io/)  
[Refactoring UI](https://refactoringui.com/book/)  
[Don't Make Me Think](https://www.amazon.com/dp/0321965515/)  

My profile on [CodeWars](https://www.codewars.com/users/maciekChmura/stats)  

**Meetups/conferences** attended.  
[FuckITup Nights #1 Kraków](https://codete.com/blog/fuckitup-nights-1-krakow/)  
[KrakowJS](https://www.meetup.com/KrakowJS/)  
[App.js conf](https://appjs.co/)  
[FrontClouders Meetup](https://codete.com/blog/frontclouders-meetup-in-krakow/)