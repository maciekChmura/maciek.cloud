---
title: "About me"
template: "page"
slug: '/pages/about'
---

Hi, I'm Maciek ☁️ . Thanks for stopping by :)  
I like to make useful things with code, particularly with React and Node.  
I've discovered that programming is the best way to express my creativity.

Now, I live and work in Krakow (Poland) as a Software Engineer.

Before that, I was an Architect. I like Typography and clear UX - skills I developed at the College of Architecture + Design.

I also designed and made a few snowboards and surfboards (funny story).  

More details can be found in my [CV](https://www.dropbox.com/sh/ly4m0gqdxeuo5kc/AAAMstOwVB6BNP0hDTKFStbBa).

Sometimes I cross publish on [DEV](https://dev.to/maciekchmura).  
If you ever find yourself nearby, let's grab a beer and chat about building things and solving problems :)
