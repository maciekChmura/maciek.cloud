---
title: "Projects"
template: "page"
slug: '/pages/projects'
---

#### timeCube [WIP] 
A pet project I am currently working on.  
> In the construction industry, design companies struggle to evaluate if their projects bring profit.  
> Architectural and Civil Engineers work on multiple projects in the span of a few months to a few years. Team members jump from one task to another.  
> It is extremely hard to know how much hours and how many people worked on a plan.  

By making a crossover of a time tracker with a simple project management and visualization app I want to help teams to make better decisions with their business.
*React + Express + PostgreSQL*

#### [React mentoring at Epam](https://github.com/maciekChmura/react-js-mentoring)  
This project helped me to understand how to create a front-end app with JavaScript. When doing this project I had a strong understanding of React. My mentor helped me to look at things from a new perspective and introduced me to some tooling from React ecosystem.  
Some of the topics covered:
- React.createElement, React.Component, React.PureComponent, functional components
- Webpack setup
- Components. Smart/Dumb. ErrorBoundary.
- Testing with Enzyme and Jest. Snapshot testing. Cypress.
- Redux
- Routing
- Eslint, Styled Components.

#### [Node mentoring at Epam](https://github.com/maciekChmura/node-js-mentoring)  
This project helped me to understand how to create back-end with JavaScript. I had an amazing mentor who taught me not only about servers but about programming in general.  
Some of the topics covered:
- NodeJS Events
- Async development
- Command Line. Debugging. Errors handling
- Filesystem and Streams
- Asynchronous Network API
- HTTP Module
- Middleware. Frameworks
- Authorization. Validation. Security
- SQL Databases. ORM
- NoSQL Databases. ODM
- NodeJS Testing
- Deploy and Tools
- Swagger. Docs

#### [CS50](https://github.com/maciekChmura/CS50-problem-sets)  
Its the best thing that happened to me since I started programming. I can't praise it enough.  
Here are the topics that are coved in the scope of this course.
- Week 1: Introduction to programming in C language. Create 2 CLI programs in C.
- Week 2: Big O, Sorting Algorithms, Binary Search, Recursion. Create 2 cyphering CLI programs with C.
- Week 3: Call Stack, Pointers, Dynamic Memory Allocation. Create a program to manipulate images in C
- Week 4: Structures, Custom Types, Singly-Linked Lists, Hash Tables, Tries, Stack, Queues. Create a spell checker in C.
- Week 5: IP, TCP, HTTP, HTML, CSS
- Week 6: Dynamic Programming, Introduction to Python.
- Week 7: Servers, Python and Flask. Create 2 CLI programs in Python.
- Week 8: Flask, MVC, SQL. Implement a stock-trading website.
- Week 9: JavaScript, DOM, Ajax. Implement a news map app.
- Week 10 and 11: Final Project

#### [feedback generator](https://feedback-generator-21432.firebaseapp.com/)  
The final project for Harvard's CS50 course. Evaluate your team members performance and generate random feedback from a set of sentences.  
*React + Firebase*
